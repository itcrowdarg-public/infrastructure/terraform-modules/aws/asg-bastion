# ASG - Bastion

## Usage

```terraform
module "bastion" {
  source = "git::https://gitlab.com/itcrowdarg-public/infrastructure/terraform-modules/aws/asg-bastion?ref=X.Y"

  vpc                   = module.vpc
  bastion_instance_type = var.bastion_instance_type
  rsa_private_key       = tls_private_key.rsa_private_key
  gitlab_users_repo_url = var.gitlab_users_repo_url
  project_name          = var.project_name
}
```