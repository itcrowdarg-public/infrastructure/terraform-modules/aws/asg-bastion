output "security_group_bastion_allow_ssh_id" {
  description = "Security group used on bastion to allow ssh access"
  value       = aws_security_group.bastion_allow_ssh.id
}
