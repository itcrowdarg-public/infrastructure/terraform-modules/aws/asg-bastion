#!/bin/bash
sudo apt update -y
sudo apt upgrade -y

sudo apt install -y mysql-client docker.io
sudo groupadd docker
sudo usermod -aG docker ubuntu

mkdir -p /home/ubuntu/.ssh
chown ubuntu:ubuntu -R /home/ubuntu/.ssh

# Authorized keys
touch /opt/update_authorized_keys

echo '#!/bin/bash' >> /opt/update_authorized_keys
echo 'date' >> /opt/update_authorized_keys
echo '' >> /opt/update_authorized_keys
echo 'docker pull itcrowdarg/bastion-access-manager:latest' >> /opt/update_authorized_keys
echo 'docker run --rm -v /opt:/opt -e PROJECT_NAME=${project_name} -e GIT_URL=${gitlab_users_repo_url} itcrowdarg/bastion-access-manager:latest' >> /opt/update_authorized_keys
echo 'if [ -f /opt/authorized_keys ]; then' >> /opt/update_authorized_keys
echo '  chmod 600 /opt/authorized_keys' >> /opt/update_authorized_keys
echo '  chown ubuntu:ubuntu /opt/authorized_keys' >> /opt/update_authorized_keys
echo '  mv /opt/authorized_keys /home/ubuntu/.ssh' >> /opt/update_authorized_keys
echo 'else' >> /opt/update_authorized_keys
echo '  echo "No authorized keys file found"' >> /opt/update_authorized_keys
echo 'fi' >> /opt/update_authorized_keys

chmod +x /opt/update_authorized_keys

/opt/update_authorized_keys &> /var/log/update_authorized_keys.log

# Cron job
echo '0 * * * * root /opt/update_authorized_keys > /var/log/update_authorized_keys.log  2>&1' >> /etc/crontab

# Private key to access ECS docker cluster
echo "${rsa_private_key}" > id_rsa
chmod 600 id_rsa
chown ubuntu:ubuntu id_rsa
mv id_rsa /home/ubuntu/.ssh
