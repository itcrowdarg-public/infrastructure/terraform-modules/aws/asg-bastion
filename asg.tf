data "template_file" "bastion_user_data" {
  template = file("${path.module}/user-data/bastion.tpl")

  vars = {
    rsa_private_key       = var.rsa_private_key.private_key_pem
    gitlab_users_repo_url = var.gitlab_users_repo_url
    project_name          = var.project_name
  }
}

resource "aws_launch_configuration" "bastion" {
  name_prefix   = "bastion-"
  instance_type = var.bastion_instance_type
  image_id      = data.aws_ami.ubuntu.id

  user_data = data.template_file.bastion_user_data.rendered

  security_groups             = [aws_security_group.bastion_allow_ssh.id]
  associate_public_ip_address = true

  lifecycle {
    create_before_destroy = true
    ignore_changes        = [image_id]
  }
}


resource "aws_autoscaling_group" "bastions" {

  name_prefix         = "bastion-"
  vpc_zone_identifier = var.vpc.public_subnets

  launch_configuration = aws_launch_configuration.bastion.name

  desired_capacity = 1
  max_size         = 1
  min_size         = 1

  protect_from_scale_in = false

  termination_policies = ["OldestInstance"]

  tag {
    key                 = "Name"
    value               = "Bastion"
    propagate_at_launch = true
  }

  lifecycle {
    create_before_destroy = true
  }

}
