variable "vpc" {
  description = "VPC where the ASG is created"
}

variable "bastion_instance_type" {
  description = "Instance type for Bastion hosts"
  type        = string
}

variable "rsa_private_key" {
  description = "RSA private key - it is installed on bastion to access docker cluster hosts"
}

variable "gitlab_users_repo_url" {
  description = "Gitlab repo with allowed users"
  type        = string
}

variable "project_name" {
  description = "Project name, used for tagging and notifications, downcase, no spaces, no underscores (DNS-friendly)"
  type        = string
}
